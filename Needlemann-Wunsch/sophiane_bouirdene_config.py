#
# Fichier de configuration
#

# Dictionnaire de score pour initialiser la matrice de substitution des nucleiques
MATRICE_SUBST = {
                  ('A', 'A'): 2,
                  ('G', 'G'): 2,
                  ('T', 'T'): 2,
                  ('C', 'C'): 2,
                  ('U', 'U'): 2,
                  ('A', 'G'): 1,
                  ('A', 'T'): -1,
                  ('A', 'C'): -1,
                  ('A', 'U'): -1,
                  ('G', 'T'): -1,
                  ('G', 'C'): -1,
                  ('G', 'U'): -1,
                  ('T', 'C'): 1,
                  ('T', 'U'): 2,
                  ('C', 'U'): 1
                }

# Valeurs des pénalités de gap d'ouverture et d'extension
DEFAUT_PENA_GAPS = {
                     "ouverture": -10,
                     "extension": -1
                   }

# Fichiers FASTA contenant les sequences à comparer
FICHIER_SEQ1 = "sophiane_bouirdene_seq1.fasta"
FICHIER_SEQ2 = "sophiane_bouirdene_seq2.fasta"
