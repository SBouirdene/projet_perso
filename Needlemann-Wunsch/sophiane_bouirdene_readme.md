# Projet Needleman et Wooch

<!-- MDTOC maxdepth:6 firsth1:1 numbering:undefined flatten:0 bullets:1 updateOnSave:1 -->

- [Projet Needleman et Wooch](#projet-needleman-et-wooch)   
   - [Auteur](#auteur)   
   - [Description](#description)   
   - [Installation](#installation)   
         - [Prérequis :](#prérequis)   
   - [Utilisation](#utilisation)   
      - [Entrées](#entrées)   
      - [configuration](#configuration)   
      - [Lancement](#lancement)   
      - [Sorties](#sorties)   
         - [Exemple de sorties](#exemple-de-sorties)   
         - [Analyse de sorties](#analyse-de-sorties)   

<!-- /MDTOC -->


## Auteur
Sophiane Bouirdene

## Description
Ce programme a pour but d'aligner deux séquences entres elles.  
Ces deux séquences peuvent être des séquences protéiques ou nucléiques.  
L'utilisateur peut choisir le type d'alignement : global(Needleman et Wunch) ou local(Smith et Watermann).

## Installation
#### Prérequis :
- Python 3.4 (minimum) est nécessaire.  
Il peut être télécharger depuis le [site officiel](https://www.python.org/downloads/release/python-372/)

- Modules tiers nécessaires:
    - numpy
    - biopython+

Ils peuvent être installés via l'outil *pip*:
```sh
pip install biopython numpy
```

## Utilisation
### Entrées
Les séquences à aligner doivent être contenues dans des fichiers FASTA.  
Les conventions suivantes s'appliquent:  
- Chaque fichier doit contenir une unique séquence (Sinon une unique séquence -aléatoire- sera extraite de chaque fichier...)
- Les séquences nucléiques sont supposés étant composés uniquement de A T C G U
- Pour les séquences protéiques, tous les acides aminés sont traités

Voici un exemple de fichier Fasta:  
```
>Pierre
GGATCGA
```
Deux fichiers d'exemple sont fournis dans le présent répertoire.

### configuration

Avant d'éviter de longues saisies interactives qui peuvent devenir frustrantes, la configuration s'effectue grâce à un fichier de configuration appelé *sophiane_bouirdene_config.py*.    
Les valeurs sont aisément modifiables dans ce fichier: Il suffit de modifier les valeurs déjà présentes.  
Les paramètres suivants sont disponibles:
- *MATRICE_SUBST*: Dictionnaire établissant le score pour les alignements des acides nucléiques  
- *DEFAUT_PENA_GAPS*: Dictionnaire des scores de gap (ouverture et extension)
- *FICHIER_SEQ1*: Nom du fichier fasta contenant la première sequence
- *FICHIER_SEQ2*: Nom du fichier fasta contenant la deuxième sequence  

**Par Défaut**
Les valeurs de la matrice de substitution nucléique sont:
- match : 2
- transition : 1
- tranversion : -1  

Pour les séquences protéiques, la matrice de Substitution utilisée est fixée à Blossom62 (Il serait aisé d'utiliser une autre matrice de référence disponible dans biopython ou personnalisée).  

le score des gaps est de :
- une ouverte : -10
- une extension : -1

### Lancement

En prenant soin de rendre le fichier exécutable, il suffit de lancer le script python sans paramètres:
```
./sophiane_bouirdene_nw.py
```

### Sorties

#### Exemple de sorties
```
Voulez vous travailler sur des protéines ou de l'ADN ? Tapez  0 pour de l'ADN ou 1 pour acides aminés
Votre Choix: 1
Quel algorithme voulez vous utiliser ? Tapez 0 pour Needleman-Wunsch ou 1 pour Smith-Waterman
Votre Choix: 1
Nombre de matrices calculées: 88
Nombre de matrices menant au score max: 88
Possibilité d'alignement 1 pour score final 15:
Matrice de score:
[[ 0  0  0  0  0  0  0  0  0  0  0  0]
 [ 0  6  0  0  0  0  0  0  6  0  0  0]
 [ 0  6  6  0  0  0  0  0  6  4  0  0]
 [ 0  0 10 10  0  0  0  4  0  6  4  4]
 [ 0  0  0 10 15  5  4  3  2  5 11  4]
 [ 0  0  0  0  9 14 14  4  3  2  4 11]
 [ 0  6  0  0  0  7 11 14 10  1  0  4]
 [ 0  0 10  4  0  0  7 15 14 10  1  4]]
Matrice de chemin:
[['' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'H' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'V' 'D' 'D' 'H' 'H' 'H' 'H' 'D' 'D' 'D']
 ['V' 'D' 'D' 'V' 'D' 'D' 'D' 'H' 'H' 'H' 'D' 'D']
 ['V' 'D' 'D' 'D' 'V' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']]
	GAAT
	|.||
	GGAT
	match: 3  Mismatch: 1 gap: 0
----------------------------
Possibilité d'alignement 2 pour score final 15:
Matrice de score:
[[ 0  0  0  0  0  0  0  0  0  0  0  0]
 [ 0  6  0  0  0  0  0  0  6  0  0  0]
 [ 0  6  6  0  0  0  0  0  6  4  0  0]
 [ 0  0 10 10  0  0  0  4  0  6  4  4]
 [ 0  0  0 10 15  5  4  3  2  5 11  4]
 [ 0  0  0  0  9 14 14  4  3  2  4 11]
 [ 0  6  0  0  0  7 11 14 10  1  0  4]
 [ 0  0 10  4  0  0  7 15 14 10  1  4]]
Matrice de chemin:
[['' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'H' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'V' 'D' 'D' 'H' 'H' 'H' 'H' 'D' 'D' 'D']
 ['V' 'D' 'D' 'V' 'D' 'D' 'D' 'H' 'H' 'H' 'D' 'D']
 ['V' 'D' 'D' 'D' 'V' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']]
	GAATTCA
	|.||..|
	GGATCGA
	match: 4  Mismatch: 3 gap: 0
----------------------------
Nombre d'alignements distincts: 2
```

#### Analyse de sorties
```
Voulez vous travailler sur des protéines ou de l'ADN ? Tapez  0 pour de l'ADN ou 1 pour acides aminés
Votre Choix: 1
```
Ici il est demandé sur quel type d'éléments nous travaillons : acides nucléiques ou acides aminés  
Dans cet exemple il a été choisi de travailler sur une séquence protéique  

```
Quel algorithme voulez vous utiliser ? Tapez 0 pour Needleman-Wunsch ou 1 pour Smith-Waterman
Votre Choix: 1
```
Il est demandé quel type d'alignement nous voulons utiliser : global ou local  
il a été décidé de travailler sur un alignement local  

```
Nombre de matrices calculées: 88
Nombre de matrices menant au score max: 88

```
C'est un résumé de notre travail sur les matrices  
la première ligne indique le nombre de (couple de) matrices créées, à cause d'égalités de score dans certaines cases: 88  
la seconde ligne indique le nombre de matrices ayant le même score maximal: 88  

```
Possibilité d'alignement 1 pour score final 15:
Matrice de score:

[[ 0  0  0  0  0  0  0  0  0  0  0  0]
 [ 0  6  0  0  0  0  0  0  6  0  0  0]
 [ 0  6  6  0  0  0  0  0  6  4  0  0]
 [ 0  0 10 10  0  0  0  4  0  6  4  4]
 [ 0  0  0 10 15  5  4  3  2  5 11  4]
 [ 0  0  0  0  9 14 14  4  3  2  4 11]
 [ 0  6  0  0  0  7 11 14 10  1  0  4]
 [ 0  0 10  4  0  0  7 15 14 10  1  4]]
```
C'est la matrice score indiquant le score maximal de chaque alignement possible à chacun des points.  
Dans le cas de Smith et Waterman, le score ne peut pas être négatif, à noter que ce n'est pas le cas pour l'alignement de Needleman et Wunsch...  
```
Matrice de chemin:
[['' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'H' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'V' 'D' 'D' 'H' 'H' 'H' 'H' 'D' 'D' 'D']
 ['V' 'D' 'D' 'V' 'D' 'D' 'D' 'H' 'H' 'H' 'D' 'D']
 ['V' 'D' 'D' 'D' 'V' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']]
```
C'est la matrice de traceback, elle indique le chemin optimal pour chaque case:  
les chemins possibles sont :
- H : Horizontal
- V : vertical
- D : diagonal  
```
	GAAT
	|.||
	GGAT
	match: 3  Mismatch: 1 gap: 0
```
C'est un des alignements locaux optimaux entre la séquence 1 et 2, ainsi que la représentation graphique du type de leur d'alignements  
Il y a aussi un résumé de la qualité de l'alignement.  

Les symboles entre les séquences représentent la qualité de l'alignement:
* pour acides nucléiques
```
 '|'  match  
 ':'  transition
 '.'  tranversion
 ' '  gap
```

* pour acides aminés
```
 '|'  match  
 '.'  mismatch
 ' '  gap
```

```
Possibilité d'alignement 2 pour score final 15:
Matrice de score:
[[ 0  0  0  0  0  0  0  0  0  0  0  0]
 [ 0  6  0  0  0  0  0  0  6  0  0  0]
 [ 0  6  6  0  0  0  0  0  6  4  0  0]
 [ 0  0 10 10  0  0  0  4  0  6  4  4]
 [ 0  0  0 10 15  5  4  3  2  5 11  4]
 [ 0  0  0  0  9 14 14  4  3  2  4 11]
 [ 0  6  0  0  0  7 11 14 10  1  0  4]
 [ 0  0 10  4  0  0  7 15 14 10  1  4]]
Matrice de chemin:
[['' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H' 'H']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'H' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'V' 'D' 'D' 'H' 'H' 'H' 'H' 'D' 'D' 'D']
 ['V' 'D' 'D' 'V' 'D' 'D' 'D' 'H' 'H' 'H' 'D' 'D']
 ['V' 'D' 'D' 'D' 'V' 'D' 'D' 'D' 'D' 'D' 'D' 'D']
 ['V' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D' 'D']]
	GAATTCA
	|.||..|
	GGATCGA
	match: 4  Mismatch: 3 gap: 0
```
C'est un second alignement optimal mais d'un chemin différent.  
```
Nombre d'alignements distincts: 2
```
rappelle le nombre de chemins distincts optimums
