#!/usr/bin/env python3
#
# coding: utf-8
#

from enum import Enum
import numpy as np
from Bio.SubsMat import MatrixInfo
import copy
from sophiane_bouirdene_config import MATRICE_SUBST, DEFAUT_PENA_GAPS, FICHIER_SEQ1, FICHIER_SEQ2


class TypeSeq(Enum):
    NUCLEIQUES = 1
    PROTEINES = 2


TYPES = {
         TypeSeq.NUCLEIQUES: ["A", "G", "T", "C", "U"],
         TypeSeq.PROTEINES: ["A", "R", "N", "D", "C", "Q", "E", "G", "H", "I", "L", "K", "M", "F", "P", "S", "T", "W", "Y", "V"]
        }


DEFAUT_SUBST = {
                TypeSeq.NUCLEIQUES: MATRICE_SUBST,
                TypeSeq.PROTEINES: MatrixInfo.blosum62
               }


class Fasta:
    """
    Lecture de fichiers au format FASTA

    Exemple d'utilisation:
       f = Fasta("fichier.fasta")
       for k,v in iteritems(f.sequences):
           print("{}: {}".format(k, v))
    """

    def __init__(self, fichier):
        nom = ""
        self._dicosequences = {}

        with open(fichier, 'r') as f:
            index = f.readline()
            while index != "":
                if index[0] == '>':
                    nom = index[1:].strip()
                    self._dicosequences[nom] = ""
                else:
                    self._dicosequences[nom] += index
                index = f.readline()

    def _get_sequences(self):
        return self._dicosequences
    sequences = property(fget=_get_sequences, doc="Dictionnaire de séquences")


class NeedlemanWunsch:
    """Implémentation de l'algorithme de Needleman et Wunsch"""

    def __init__(self, type):
        """ Constructeur de la classe NeedlemanWunsch

        Arg:
            type(TypeSeq): Enuméré donnant la nature des élèments

        """
        self._type = type
        self._MATRICE_SUBST = self._creer_MATRICE_SUBST(TYPES[type], DEFAUT_SUBST[type])
        self._pena_gaps = DEFAUT_PENA_GAPS

    def _creer_MATRICE_SUBST(self, types, valeurs):
        """Initialise la matrice de score et la matrice de traceback

        Args:
            types(str) : liste des élèments étudiés : Acides aminés ou acides nucléiques
            valeurs(dict) : dictionnaire de score (int) pour les gaps
        Retour:
            Matrice de substitution
        """

        tailleliste = len(types)
        matrice = np.zeros((tailleliste, tailleliste), dtype=int)
        for ligne in range(tailleliste):
            for colonne in range(tailleliste):
                if (types[colonne], types[ligne]) in valeurs.keys():
                    val = valeurs[(types[colonne], types[ligne])]
                elif (types[ligne], types[colonne]) in valeurs.keys():
                    val = valeurs[(types[ligne], types[colonne])]
                else:
                    raise KeyError("({}, {}) not found".format(types[ligne], types[colonne]))
                matrice[ligne][colonne] = val
        return matrice

    def _init_matrices(self, cols, lignes):
        """Initialise la matrice de score et la matrice de traceback

        Args:
            cols(int): nombre de la colonnes pour les matrices
            lignes(int): nombre de lignes pour les matrices

        Retour:
            Tuple de 2 éléments: matrice de score(int) et matrice traceback(str)
        """
        matrice_score = np.zeros((lignes, cols), dtype=int)
        matrice_chemin = np.zeros((lignes, cols), dtype=str)
        for i in range(cols - 1):
            matrice_score[0][i + 1] = self._pena_gaps["ouverture"] + (self._pena_gaps["extension"] * i)
            matrice_chemin[0][i + 1] = "H"
        for j in range(lignes - 1):
            matrice_score[j + 1][0] = self._pena_gaps["ouverture"] + (self._pena_gaps["extension"] * j)
            matrice_chemin[j + 1][0] = "V"
        return (matrice_score, matrice_chemin)

    def _score_substitution(self, base1, base2):
        """Calculer le score de l'alignement.

        Args:
            base1(str): élement de la séquence1 à étudier ( un Acides Aminé ou Acide Nucléique)
            base2(str): élement de la séquence2 à étudier ( un Acides Aminé ou Acide Nucléique)

        Retour:
            Le score de la matrice de substitution associé au couple (base1, base2)
        """

        base1_temp = base1.upper()
        base2_temp = base2.upper()
        ligne = 0
        colonne = 0
        # Rmq: Pas de garde sur l'index de la liste_type car on est sur de trouver
        while base1_temp != TYPES[self._type][ligne]:
            ligne += 1
        while base2_temp != TYPES[self._type][colonne]:
            colonne += 1
        return self._MATRICE_SUBST[ligne][colonne]

    def _choisir_valeur(self, diag, verti, hori):
        """Sélectionne le(s) score(s) et la(es) direction(s)

        Args:
            diag(int): Valeur diagonale
            verti(int): Valeur verticale
            hori(int): Valeur horizontale

        Retour:
            Tuple de 2 éléments: valeur(int), liste de direction(str)
        """
        dirs = []
        val = max(diag, verti, hori)
        if diag == val:
            dirs.append("D")
        if verti == val:
            dirs.append("V")
        if hori == val:
            dirs.append("H")
        return (val, dirs)

    def _calculer_matrices(self, seq1, seq2, matrices, debut=(1, 1)):
        """Remplit les matrices de score et de traceback

        Args:
            seq1(str): sequence à étudier 1
            seq2(str): sequence à étudier 2
            matrices(int,str): tuple de matrices score et traceback
            debut():

        Retour:
            Tuple de 2 éléments: matrice score et matrice traceback remplies
        """
        retour = []
        matrice_score = matrices[0]
        matrice_chemin = matrices[1]
        for ligne in range(1, matrice_score.shape[0]):
            for colonne in range(1, matrice_score.shape[1]):
                if (ligne < debut[0]):
                    continue
                if (ligne == debut[0]) and (colonne < debut[1]):
                    continue
                diag = matrice_score[ligne - 1][colonne - 1] + self._score_substitution(seq1[colonne - 1], seq2[ligne - 1])
                if matrice_chemin[ligne-1][colonne] == "V":
                    verti = matrice_score[ligne - 1][colonne] + self._pena_gaps["extension"]
                    hori = matrice_score[ligne][colonne - 1] + self._pena_gaps["ouverture"]
                elif matrice_chemin[ligne][colonne-1] == "H":
                    verti = matrice_score[ligne - 1][colonne] + self._pena_gaps["ouverture"]
                    hori = matrice_score[ligne][colonne - 1] + self._pena_gaps["extension"]
                else:
                    verti = matrice_score[ligne - 1][colonne] + self._pena_gaps["ouverture"]
                    hori = matrice_score[ligne][colonne - 1] + self._pena_gaps["ouverture"]
                score, dirs = self._choisir_valeur(diag, verti, hori)
                matrice_score[ligne][colonne] = score
                matrice_chemin[ligne][colonne] = dirs[0]
                if (len(dirs) > 1):
                    # Plusieurs directions conduisent à la même valeur: Génération de(s) matrice(s) alternatives
                    deb_colonne = colonne + 1
                    deb_ligne = ligne
                    if deb_colonne >= matrice_score.shape[1]:
                        deb_colonne = 1
                        deb_ligne += 1
                    for dir in dirs[1:]:
                        nouv_matrice_score = copy.deepcopy(matrice_score)
                        nouv_matrice_chemin = copy.deepcopy(matrice_chemin)
                        nouv_matrice_chemin[ligne][colonne] = dir
                        if deb_ligne < matrice_score.shape[0]:
                            retour += self._calculer_matrices(seq1, seq2, (nouv_matrice_score, nouv_matrice_chemin), (deb_ligne, deb_colonne))
                        else:
                            # Ce cas survient si les chemins égaux surviennent sur la dernière case (bas,droite)
                            retour += [(nouv_matrice_score, nouv_matrice_chemin)]

        retour.append((matrice_score, matrice_chemin))
        return retour

    def _point_depart(self, matrices):
        """Retourne les coordonnées du point de départ pour le 'traceback'

        Rmq: Pour Needleman et Wunsch, il n'y en à qu'un mais, il peut y en avoir plusieurs
             pour Smith et Waterma (c'est pourquoi on renvoie une liste)

           Args:
                matrices(tuple): tuple de matrices (score, chemin)

           Retour:
                Liste de tuple de 2 éléments: ligne(int), colonne(str)
        """
        # Pour Needleman et Wunsch, un unique point de départ: l'élément en bas à droite:
        return [(matrices[0].shape[0]-1, matrices[0].shape[1]-1)]

    def _parcours_fini(self, matrices, coords):
        """ Indique si le parcours du chemin est achevé

            Args:
                matrices(tuple): tuple de matrices (score, chemin)
                coords(tuple): tuple de coordonnées (ligne, colonne)

            Retour:
                Booleen indiquant si on a fini l'alignement
        """
        # Pour Needleman et Wunsch, le parcours est fini quand nous sommes en haut à gauche
        return (coords[0] == 0) and (coords[1] == 0)

    def _filtrer_max(self, candidats):
        """Filtre les matrices possibles et ne garde que la(les) matrices ayant le meilleur score final.

            Args:
                candidats(list): tuple de matrices (score, chemin)

            Retour:
                tuple de deux éléments:
                    - liste de tuple de matrices score et traceback
                    - liste des résultats
        """
        val_max = None
        maxs = []
        for matrices in candidats:
            ligne, colonne = self._point_depart(matrices)[0]
            val = matrices[0][ligne][colonne]
            if not val_max:
                val_max = val
                maxs = [matrices]
            elif (val > val_max):
                val_max = val
                maxs = [matrices]
            elif (val == val_max):
                maxs.append(matrices)
        return (val_max, maxs)

    def _calculer_chemin(self, matrices):
        """Calcule les chemins possible pour un couple de matrice (score, chemin) donné

        Rmq: Pour Needleman et Wunsch, il n'y en à qu'un mais, il peut y en avoir plusieurs
             pour Smith et Waterma (c'est pourquoi on renvoie une liste)

        Args:
            matrices(int, str): matrices score et traceback

        Retour:
            Une liste de dictionnaires contenant:
                    - les cordonnées de départ (clé "depart")
                    - une chaine de parcours (clé "directions")
        """
        retour = []
        matrice_chemin = matrices[1]

        for ligne, colonne in self._point_depart(matrices):
            depart = (ligne, colonne)
            chemin = ""
            while not self._parcours_fini(matrices, (ligne, colonne)):
                direction = matrice_chemin[ligne][colonne]
                chemin += direction
                if direction == "D":
                    ligne -= 1
                    colonne -= 1
                elif direction == "V":
                    ligne -= 1
                elif direction == "H":
                    colonne -= 1
            retour.append({"depart": depart, "directions": chemin})
        return retour

    def _afficher_chemin(self, seq1, seq2, chemin):
        """ Affiche l'alignement correspondant à un chemin:
                Sequence 1 et 2 alignées et une représentation graphique de la qualité de l'alignement

            Args:
                seq1(str): sequence 1
                seq2(str): sequence 2
                chemin(dict): dictionnaire tel que renvoyé par la fonction _calculer_chemin
        """
        pyrimidine = []
        purine = []
        if self._type is TypeSeq.NUCLEIQUES:
            pyrimidine = ["G", "g", "T", "t", "U", "u"]
            purine = ["A", "a", "C", "c"]

        sequencealign1 = ""
        sequencealign2 = ""
        sequencealignement = ""
        ligne, colonne = chemin["depart"]
        for direction in chemin["directions"]:
            val1 = seq1[colonne - 1]
            val2 = seq2[ligne - 1]
            if direction == "D":
                sequencealign2 = val2 + sequencealign2
                sequencealign1 = val1 + sequencealign1
                if val2.upper() == val1.upper():
                    sequencealignement = "|" + sequencealignement
                elif val2 in purine and val1 in purine:
                    sequencealignement = ":" + sequencealignement
                elif val2 in pyrimidine and val1 in pyrimidine:
                    sequencealignement = ":" + sequencealignement
                else:
                    sequencealignement = "." + sequencealignement
                ligne -= 1
                colonne -= 1
            elif direction == "V":
                sequencealign1 = "-" + sequencealign1
                sequencealign2 = val2 + sequencealign2
                sequencealignement = " " + sequencealignement
                ligne -= 1
            elif direction == "H":
                sequencealign2 = "-" + sequencealign2
                sequencealign1 = val1 + sequencealign1
                sequencealignement = " " + sequencealignement
                colonne -= 1
        match = sequencealignement.count("|")
        gap = sequencealignement.count(" ")
        transission = sequencealignement.count(":")
        tranversion = sequencealignement.count(".")
        mismatch = transission + tranversion
        print("\t" + sequencealign1)
        print("\t" + sequencealignement)
        print("\t" + sequencealign2)
        if self._type is TypeSeq.NUCLEIQUES:
            print("\tmatch: {} Mismatch: {} (transversion: {} transition: {}) gap: {}".format(match, mismatch, tranversion, transission, gap))
        else:
            print("\tmatch: {}  Mismatch: {} gap: {}".format(match, mismatch, gap))
        print("----------------------------")

    def _afficher_chemins(self, seq1, seq2, liste_max, score_max):
        """Affiche les chemins possibles en filtrant les alignements identiques
            Args:
                seq1(str): sequence 1
                seq2(str): sequence 2
                liste_max(list): liste de tuples de matrices de score et de traceback ayant le même score max
                score_max(int): valeur du score maximum

            Retour:
                Le nombre d'alignements distinct obtenus à partir des matrices
        """
        num = 1
        chemins_affiches = []
        for matrices in liste_max:
            chemins = self._calculer_chemin(matrices)
            for chemin in chemins:
                if chemin not in chemins_affiches:
                    chemins_affiches.append(chemin)
                    print("Possibilité d'alignement {} pour score final {}:".format(num, score_max))
                    print("Matrice de score:")
                    print(matrices[0])
                    print("Matrice de chemin:")
                    print(matrices[1])
                    self._afficher_chemin(seq1, seq2, chemin)
                    num += 1
        return num - 1

    def _verifier_validite(self, seq):
        """ Verifie que tous les elements dans notre séquence existent bel et bien dans notre liste Type utilisée .

            Args:
                seq(str): une séquence à tester

            Retour:
                - Soit une chaine s'il n'y a pas d'erreur
                - Soit le caractère fautif en cas d'anomalie
        """
        for type in seq:
            if type.upper() not in TYPES[self._type]:
                return type
        return ""

    def aligner(self, seq1, seq2):
        """ Lance un calcul sur deux séquences.

            Args:
                seq1(str): première sequence à aligner
                seq2(str): deuxième sequence à aligner
        """
        for seq in [seq1, seq2]:
            res = self._verifier_validite(seq)
            if res != "":
                print("La sequence {} contient {} qui n'est pas un type attendu".format(seq, res))
                print("Veuillez corriger...")
                return
        cols = len(seq1) + 1
        lignes = len(seq2) + 1
        matrices = self._init_matrices(cols, lignes)
        candidats = self._calculer_matrices(seq1, seq2, matrices)
        print("Nombre de matrices calculées: {}".format(len(candidats)))
        liste_max = []
        score_max, liste_max = self._filtrer_max(candidats)
        print("Nombre de matrices menant au score max: {}".format(len(liste_max)))
        alignements = self._afficher_chemins(seq1, seq2, liste_max, score_max)
        print("Nombre d'alignements distincts: {}".format(alignements))


class SmithWaterman(NeedlemanWunsch):
    """Implémentation de l'algorithme de Swith et Waterman

       Note de conception: Cette classe hérite de NeedlemanWunsch et surcharge
                           uniquement les méthodes qui doivent être spécifiques
    """

    def _init_matrices(self, cols, lignes):
        """Initialise la matrice de score et la matrice de traceback

        Args:
            cols(int): nombre de la colonnes pour les matrices
            lignes(int): nombre de lignes pour les matrices

        Retour:
            Tuple de 2 éléments: matrice de score(int) et matrice traceback(str)
        """
        # Pour Swith et Waterman la première ligne et colonne sont conservées à 0
        matrice_score = np.zeros((lignes, cols), dtype=int)
        matrice_chemin = np.zeros((lignes, cols), dtype=str)
        for i in range(cols - 1):
            matrice_chemin[0][i + 1] = "H"
        for j in range(lignes - 1):
            matrice_chemin[j + 1][0] = "V"
        return (matrice_score, matrice_chemin)

    def _choisir_valeur(self, diag, verti, hori):
        """Sélectionne le(s) score(s) et la(es) direction(s)

        Args:
            diag(int): Valeur diagonale
            verti(int): Valeur verticale
            hori(int): Valeur horizontale

        Retour:
            Tuple de 2 éléments: valeur(int), liste de direction(str)
        """
        dirs = []
        val = max(diag, verti, hori)
        if diag == val:
            dirs.append("D")
        if verti == val:
            dirs.append("V")
        if hori == val:
            dirs.append("H")
        # Spécificité de Smith et Waterman: Pas de valeur négative
        if val < 0:
            val = 0
        return (val, dirs)

    def _point_depart(self, matrices):
        """Retourne les coordonnées du point de départ pour le 'traceback'

        Rmq: Pour Needleman et Wunsch, il n'y en à qu'un mais, il peut y en avoir plusieurs
             pour Smith et Waterma (c'est pourquoi on renvoie une liste)

           Args:
                matrices(tuple): tuple de matrices (score, chemin)

           Retour:
                Liste de tuple de 2 éléments: ligne(int), colonne(str)
        """
        # Pour Smith et Waterman, c'est la valeur max
        max = 0
        coords = [(0, 0)]
        for ligne in range(matrices[0].shape[0]):
            for colonne in range(matrices[0].shape[1]):
                val = matrices[0][ligne][colonne]
                if (val > max):
                    max = val
                    coords = [(ligne, colonne)]
                elif (val == max):
                    coords.append((ligne, colonne))
        return coords

    def _parcours_fini(self, matrices, coords):
        """ Indique si le parcours du chemin est achevé

            Args:
                matrices(tuple): tuple de matrices (score, chemin)
                coords(tuple): tuple de coordonnées (ligne, colonne)

            Retour:
                Booleen indiquant si on a fini l'alignement
        """
        # Pour Smith et Waterman, le parcours est fini quand on atteint un 0
        return matrices[0][coords[0]][coords[1]] == 0


class IHM:
    def _get_val(self, question, attendus):
        """ Choisir et vérifier que le choix est correct

        Args:
               question(str): Phrase explicitant le choix à afficher
               attendus(str): Les réponses possibles
        Retour:
               str: Le choix de l'utilisateur
        """
        val = None
        print(question)
        while not val or val not in attendus:
            val = input("Votre Choix: ")
        return val

    def menu(self):
        """
        Gestion des questions interactives pour l'utilisateur

        Retour:
            Une instance de NeedlemanWunsch ou SmithWaterman

        """
        val = self._get_val("Voulez vous travailler sur des protéines ou de l'ADN ? Tapez  0 pour de l'ADN ou 1 pour acides aminés", ["0", "1"])
        if val == "0":
            type = TypeSeq.NUCLEIQUES
        else:
            type = TypeSeq.PROTEINES
        val = self._get_val("Quel algorithme voulez vous utiliser ? Tapez 0 pour Needleman-Wunsch ou 1 pour Smith-Waterman", ["0", "1"])
        if val == "0":
            algo = NeedlemanWunsch(type)
        else:
            algo = SmithWaterman(type)
        return algo


if __name__ == '__main__':
    ihm = IHM()
    algo = ihm.menu()

    # Récupère la 1ere sequence du fichier (1ere valeur du dictionnaire de séquences issu de la lecture fasta)
    seq1 = next(iter(Fasta(FICHIER_SEQ1).sequences.values()))
    seq2 = next(iter(Fasta(FICHIER_SEQ2).sequences.values()))

    algo.aligner(seq1, seq2)
