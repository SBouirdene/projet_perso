# Personal projects


## Author
Sophiane Bouirdene

## Introduction

This gitlab serves to display some of the scripts I did during my formation as personal project.

## Parts

### Needlemann-Wunsch

Directory containing all programs and test data for Needleman-Wunsch and Smith-Watermann algorithm reimplemented in python.

### utils

Contain a set of small helpfull scripts used during my different projects.
